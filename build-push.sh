REGISTRY=registry.gitlab.com/mtts10991/iot-webportal
read -p "Buid with tag: " TAG
echo "--------------------------------------------------"
echo "STEP 1 : BUILD IMAGE WITH INPUT TAG"
docker build --platform linux/amd64 -f ./Dockerfile.Nginx -t $REGISTRY:$TAG .
echo "--------------------------------------------------"
echo "STEP 2 : PUSH IMAGE"
docker push $REGISTRY:$TAG
echo "--------------------------------------------------"
read -p "FINISHED"