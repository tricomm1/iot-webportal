import { createRouter, createWebHistory } from 'vue-router'
import UserDetailViewVue from '@/views/backoffice/UserDetailView.vue'
import HomeView from '../views/HomeView.vue'
import ContactViewVue from '@/views/ContactView.vue'
import ManualViewVue from '@/views/ManualView.vue'
import ProjectViewVue from '@/views/ProjectView.vue'
import UserProjectViewVue from '@/views/backoffice/UserProjectView.vue'
import storage from '@/shared/services/storage'
import ContentViewVue from '@/views/ContentView.vue'
import SetupMenuViewVue from '@/views/backoffice/SetupMenuView.vue'
import SetupWebViewVue from '@/views/backoffice/SetupWebView.vue'
import SetupUserViewVue from '@/views/backoffice/SetupUserView.vue'
import SetupRoleViewVue from '@/views/backoffice/SetupRoleView.vue'
import SetupThemeViewVue from '@/views/backoffice/SetupThemeView.vue'
import BackOfficeViewVue from '@/views/backoffice/BackOfficeView.vue'
import SetupFaqViewVue from '@/views/backoffice/SetupFaqView.vue'
import PM25ViewVue from '@/views/dashboard/PM25View.vue'
import LoginVue from '@/components/Login.vue'
import RegisterVue from '@/components/Register.vue'
import AddDeviceVue from '@/views/AddDevice.vue'
import UserDevice from '@/views/UserDevice.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect: '/home'
    },
    {
      path: '/login',
      name: 'login',
      component: LoginVue
    },
    {
      path: '/register',
      name: 'register',
      component: RegisterVue
    },
    {
      path: '/home',
      name: 'home',
      component: HomeView
    },
    {
      path: '/project',
      name: 'project',
      component: ProjectViewVue
    },
    {
      path: '/pm25-dashboard',
      name: 'pm2.5',
      component: PM25ViewVue
    },
    {
      path: '/manuals',
      name: 'manual',
      component: ManualViewVue
    },
    {
      path: '/contact-us',
      name: 'contact-us',
      component: ContactViewVue
    },
    {
      path: '/contents/:id',
      name: 'contents',
      component: ContentViewVue
    },
    {
      path: '/add-device',
      name: 'user-add-device',
      component: AddDeviceVue
    },
    {
      path: '/device-user/:id',
      name: 'device-user-sensor',
      component: UserDevice
    },
    {
      path: '/backoffice/',
      name: 'user',
      meta: { requireAuth: true },
      redirect: { name: 'user-detail' },
      children: [
        {
          path: 'user-detail',
          name: 'user-detail',
          component: UserDetailViewVue
        },
        {
          path: 'project',
          name: 'user-project',
          component: UserProjectViewVue
        },
        {
          path: 'role',
          name: 'su-role',
          component: SetupRoleViewVue
        },
        {
          path: 'menu',
          name: 'su-menu',
          component: SetupMenuViewVue
        },
        {
          path: 'users',
          name: 'su-user',
          component: SetupUserViewVue
        },
        {
          path: 'theme',
          name: 'su-theme',
          component: SetupThemeViewVue
        },
        {
          path: 'faq',
          name: 'su-faq',
          component: SetupFaqViewVue
        },
        {
          path: 'web-config',
          name: 'su-web-config',
          component: SetupWebViewVue
        }
      ]
    },
    {
      path: "/:pathMatch(.*)*",
      redirect: '/home'
    }
  ]
})

router.beforeEach((to, from) => {
  const token = storage.get('userId');
  if (to.meta['requireAuth']) {
    if (token) {

    } else {
      return { name: 'home' };
    }
  }

})

export default router
