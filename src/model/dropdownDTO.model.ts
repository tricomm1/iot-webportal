export class DropdownDTO {
    id?: string;
    code?: string;
    name?: string;
}