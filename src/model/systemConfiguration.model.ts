export class SystemConfiguration {
    id?: string;
    configurationCode?: string;
    configurationName?: string;
    configurationValue?: string | string[];
    ordinal?: number;
    inputType?: string;
}