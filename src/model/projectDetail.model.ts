import type { UserDetail } from "./userDetail.model"

export class ProjectDetail {
    projectName?: string
    subjectCode?: string
    projectDescription?: string
    projectBanner?: string
    ownerId?: string
    dashboard?: string = ''
    createdDate?: Date;
    owner?: UserDetail
}