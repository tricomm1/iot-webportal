import type { RoleDTO } from "./roleDTO.model";

export class UserDetail {
    id?: string;
    namePrefix?: string;
    firstName?: string;
    lastName?: string;
    phoneNo?: string;
    identificationNumber?: string;
    email?: string;
    faculty?: string;
    facultyId?: string;
    department?: string;
    departmentId?: string;
    major?: string;
    majorId?: string;
    role?: RoleDTO[];
}