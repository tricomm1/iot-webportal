export class RegisterData {
    email?: string
    password?: string
    namePrefix?: string
    firstName?: string
    lastName?: string
    roleId?: string | any
    identificationNumber?: string
    faculty?: string | any
    department?: string | any
    major?: string | any
    recaptcha?: string
    constructor() {
        this.roleId = null;
        this.faculty = null;
        this.department = null;
        this.major = null;
    }
}