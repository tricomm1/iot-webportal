export class FaqDTO {
    id?: string;
    faqName?: string;
    faqLink?: string;
    ordinal?: string;
    active?: boolean;

    constructor() {
        this.active = true;
    }
}