import type { MenuDTO } from "./menuDTO.model";

export class RoleDTO {
    id?: string;
    roleCode?: string;
    roleName?: String;
    userForRegister?: boolean;
    menuAccess?: MenuDTO[] = [];
}