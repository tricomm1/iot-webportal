export class MenuDTO{
    id?: string;
    menuCode?: string;
    menuName?: string;
    menuPath?: string;
    menuPathName?: string;
}