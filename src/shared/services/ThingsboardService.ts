import axios from "axios";
const api = axios.create({
  baseURL: import.meta.env.VITE_Thingsboard_API_URL,
});

export default {
  Register(data: any) {
    return api.post("noauth/signup", data);
  },
  Login(data: any) {
    return api.post("auth/login", data);
  },
  UserInfo(token: any, email: string) {
    return api.get(
      `customerInfos/all?pageSize=1&page=0&textSearch=${email}&sortOrder=DESC`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  GetPMDashboardData(token, startTs, endTs) {
    const key = "PM2.5";
    const device = import.meta.env.VITE_Thingsboard_DEVICE;
    const interval = 60000 * 60;
    const limit = 86400;
    const agg = "NONE";
    return api.get(
      `plugins/telemetry/DEVICE/${device}/values/timeseries?keys=${key}&startTs=${startTs}&endTs=${endTs}&interval=${interval}&limit=${limit}&agg=${agg}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  GetCustomerAll(token: any, email: string) {
    return api.get(
      `customerInfos/all?pageSize=10&page=0&textSearch=${email}&sortOrder=DESC`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  GetGroup(token: string, GroupName: string) {
    // 46764fd0-c56d-11ee-8de8-732abc7bae5d ตัวที่จะ move ไป
    // 1c30d1e0-c56e-11ee-8de8-732abc7bae5d ตัวของ คนที่จะสมัคร
    return api.get(
      `entityGroupInfos/TENANT/78e41140-8293-11ee-a457-19e52dcf6b5d/CUSTOMER?pageSize=1&page=0&textSearch=${GroupName}&sortOrder=DESC`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  MoveGroup(token: string, GroupIdMove: string, IdGroupUser: string) {
    return api.post(`entityGroup/${GroupIdMove}/addEntities`, [IdGroupUser], {
      headers: { Authorization: `Bearer ${token}` },
    });
  },
  GetLastestData(
    token,
    device = import.meta.env.VITE_Thingsboard_DEVICE,
    keys = ["PM2.5"]
  ) {
    const interval = 60000 * 60;
    const limit = 10000;
    return api.get(
      `plugins/telemetry/DEVICE/${device}/values/timeseries?keys=${keys}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  GetDevices(token: any, userId: string) {
    return api.get(`customer/${userId}/devices?pageSize=10&page=0`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  GetTokenDevice(token: any, DeviceId: string) {
    return api.get(`device/${DeviceId}/credentials`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  AddDevices(token: any, data: {}) {
    return api.post(
      `device-with-credentials?entityGroupIds=`,
      { ...data },
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
  },
  DeleteDevice(token: any, IdDevice: string) {
    return api.delete(`device/${IdDevice}`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
  GetLastestDataAll(token: string, device: string) {
    const interval = 60000 * 60;
    const limit = 10000;
    return api.get(`plugins/telemetry/DEVICE/${device}/values/timeseries`, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  },
};
