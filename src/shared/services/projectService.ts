import api from './api';
import { of } from 'rxjs';

const services = 'Project';
export default {
    getRecentProjects() {
        return api.get(`${services}/getRecentProject`);
    },
    getProjectList(criteria?: any) {
        return api.post(`${services}/getProjectList`, criteria);
    },
    getProjectListByUser(userId: string) {
        return api.get(`${services}/getProjectListByUser/${userId}`);
    },
    getProjectDetail(projectId: string) {
        return api.get(`${services}/getProjectDetail/${projectId}`);
    },
    addProject(project: any) {
        return api.post(`${services}/addProject`, project)
    },
    editProject(project: any) {
        return api.post(`${services}/editProject`, project)
    },
    deleteProject(id: any) {
        return api.delete(`${services}/deletetProject?id=${id}`)
        // return api.delete(`${services}/deleteProject/${id}`)
    }
}