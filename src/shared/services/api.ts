import axios from 'axios';

const api = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
    }
})

api.interceptors.response.use(
    response => {
        if (response.status === 200 || response.status === 201) {
            return Promise.resolve(response.data);
        } else {
            return Promise.reject(response.data);
        }
    },
    error => {
        if (error.response.status) {
            if (typeof (error.response.data) === 'string') {
                return Promise.reject(error.response.data);
            }
            else {
                return Promise.reject(error.response.data.errors);
            }
        }
    }
)
export default api;