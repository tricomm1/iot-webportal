// https://trinergyxbotnoi.herokuapp.com/predict?startTS=1681689600000&endTs=1681948800000
import axios from 'axios';
import https from 'https';

const api = axios.create({
    // baseURL: import.meta.env.VITE_Thingsboard_API_URL
    baseURL: "https://tricommmachinelearninng-37dab3d5dcad.herokuapp.com/",
    // baseURL: "https://trinergyxbotnoi.herokuapp.com/",
    headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
    }
})

export default {
    GetPMDashboardPredictData(startTs : string, endTs : string) {
        return api.get(`predict?startTS=${startTs}&endTs=${endTs}`);
    }
}