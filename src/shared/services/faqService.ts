import type { FaqDTO } from "@/model/faqDTO.model";
import api from './api';

const services = 'Faq';
export default {
    getFaqList() {
        return api.get(`${services}/GetFaq`);
    },
    getFaqbyName(name: string) {
        return api.get(`${services}/getFaqbyName/${name}`);
    },
    getFaqById(id: string) {
        return api.get(`${services}/getFaqById/${id}`);
    },
    AddFaq(faq: FaqDTO) {
        return api.post(`${services}/AddFaq`, faq);
    },
    UpdateFaq(faq: FaqDTO) {
        return api.post(`${services}/UpdateFaq`, faq);
    },
    DeleteFaq(faqId: string) {
        return api.delete(`${services}/DeleteFaq/${faqId}`);
    }
}