import type { UserDetail } from '@/model/userDetail.model';
import { Subject } from 'rxjs';
import Api from './api';
import storage from './storage';

const services = 'User';
var $currentUser: Subject<UserDetail | null> = new Subject();
var currentUser: UserDetail | null;
var $triggerLogin: Subject<boolean | null> = new Subject();
export default {
    setCurrentUser(user: UserDetail | null) {
        currentUser = user;
        $currentUser.next(user);
    },
    getCurrentUser() {
        return currentUser;
    },
    Login(data: any) {
        return Api.post(`${services}/Login`, data);
    },
    Logout() {
        this.setCurrentUser(null);
        return Api.get(`${services}/Logout`);
    },
    register(data: any) {
        return Api.post(`${services}/CreateUser`, data);
    },
    update(user: UserDetail) {
        return Api.post(`${services}/UpdateUser`, user);
    },
    getUserDetail(userId: string) {
        return Api.get(`${services}/GetUserDetail/${userId}`)
    },
    getUserList() {
        return Api.get(`${services}/GetUserList`)
    },

    subscribeUser() {
        return $currentUser;
    },
    subscribeLoginTrigger() {
        return $triggerLogin;
    },
}