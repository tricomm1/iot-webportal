import api from './api';

const services = 'Master';
export default {
    getDDLItemList(group: string) {
        return api.get(`${services}/GetDDLItemList/${group}`);
    },
    getFacultyList() {
        return api.get(`${services}/GetFacultyList`);
    },
    getDepartmentListByFaculty(facultyId: string) {
        return api.get(`${services}/GetDepartmentListByFaculty/${facultyId}`);
    },
    getMajorByDepartment(departmentId: string) {
        return api.get(`${services}/GetMajorListByDepartment/${departmentId}`);
    }
}