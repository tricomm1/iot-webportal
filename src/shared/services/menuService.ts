import type { MenuDTO } from '@/model/menuDTO.model';
import api from './api';

const services = 'Menu';
export default {
    getMenuList() {
        return api.get(`${services}/getMenuList`);
    },
    getMenuByUser(userId: string) {
        return api.get(`${services}/GetMenuByUser/${userId}`)
    },
    getMenuByUserRole(roleId: string) {
        return api.get(`${services}/GetMenuByUserRole/${roleId}`)
    },
    getMenuDetail(menuId: string) {
        return api.get(`${services}/getMenuDetail/${menuId}`);
    },
    addMenu(menu: MenuDTO) {
        return api.post(`${services}/AddMenu`, menu)
    },
    updateMenu(menu: MenuDTO) {
        return api.post(`${services}/UpdateMenu`, menu)
    }
}