import type { SystemConfiguration } from "@/model/systemConfiguration.model";
import { BehaviorSubject } from "rxjs";
import { ref } from "vue";
import api from "./api";

const services = "SystemConfiguration";
const config = ref<SystemConfiguration[]>([]);
const onConfigChanged: BehaviorSubject<any> = new BehaviorSubject(null);

export default {
    getSystemConfigurationTable() {
        return api.get(`${services}/GetSystemConfigurationTable`);
    },
    getSystemConfigurationData() {
        return api.get(`${services}/GetSystemConfigurationData`);
    },
    updateSystemConfigurationData(data) {
        return api.post(`${services}/UpdateSystemConfigurationData`, data)
    },
    setSystemConfigurationData(_data) {
        _data.forEach(ele => {
            if (ele.inputType === 'multiple-image') {
                if ((ele.configurationValue as string).length != 0)
                    ele.configurationValue = JSON.parse((ele.configurationValue as string))
            }
        });
        config.value = [..._data];
    },
    getConfigValue(code): any {
        return config.value.filter(x => x.configurationCode == code)[0]?.configurationValue ?? "";
    },
    config,
    onConfigChanged
}