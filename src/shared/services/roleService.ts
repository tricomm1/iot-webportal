import type { RoleDTO } from '@/model/roleDTO.model';
import api from './api';

const services = 'Role';
export default {
    getRoleList() {
        return api.get(`${services}/GetRoleList`);
    },
    getRoleListForRegister() {
        return api.get(`${services}/getRoleListForRegister`);
    },
    getRoleDetail(roleId: string){
        return api.get(`${services}/getRoleDetail/${roleId}`);
    },
    addRole(role: RoleDTO) {
        return api.post(`${services}/AddRole`, role)
    },
    updateRole(role: RoleDTO) {
        return api.post(`${services}/UpdateRole`, role)
    }
}