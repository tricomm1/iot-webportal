export enum ConfigurationCode {
    Logo = "LOGO",
    BannerTitle = "BN01",
    BannerSubTitle = "BN02",
    BannerImage = "BN03",
    BannerDashboardLink = "BN04",
    Theme = "THE001",
    ContactAddress = "CT01",
    ContactEmail = "CT02",
    ContactPhone = "CT03",
    ContactLineLink = "CT04",
    ContactLineId = "CT05",

}