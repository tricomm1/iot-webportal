import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";

import "vuetify/styles";
import { createVuetify } from "vuetify";
import * as components from "vuetify/components";
// import * as labsComponents from 'vuetify/labs/components'
import * as directives from "vuetify/directives";
import mdiVue from "mdi-vue/v3";
import * as mdijs from "@mdi/js";
import "@mdi/font/css/materialdesignicons.css";
import materialKit from "./material-kit";

import { VueReCaptcha } from 'vue-recaptcha-v3'

// IMPORT COMPONENT FROM PACKAGES
import { QuillEditor } from "@vueup/vue-quill";
import { LMap, LMarker, LTileLayer, LTooltip } from "@vue-leaflet/vue-leaflet";
import "@vueup/vue-quill/dist/vue-quill.snow.css";
import Vue3VideoPlayer from "@cloudgeek/vue3-video-player";
import "@cloudgeek/vue3-video-player/dist/vue3-video-player.css";
import VueDatePicker from "@vuepic/vue-datepicker";
import "@vuepic/vue-datepicker/dist/main.css";
// import bootstrap from "bootstrap/dist/js/bootstrap.min.js";

import "./assets/main.css";
import filters from "./filters";
import store from "./store";

import VueSweetalert2 from "vue-sweetalert2";
import "sweetalert2/dist/sweetalert2.min.css";

declare module "vue" {
  interface ComponentCustomProperties {
    $filters;
  }
}

const app = createApp(App);

const myCustomLightTheme = {
  dark: false,
  colors: {
    background: "#FFFFFF",
    surface: "#FFFFFF",
    default: "#172b4d",
    primary: "#03a9f4",
    secondary: "#fafafa",
    error: "#B00020",
    info: "#11cdef",
    success: "#2dce89",
    warning: "#fb6340",
    danger: "#f5365c",
  },
};

const vuetify = createVuetify({
  components,
  directives,
  // theme: {true},
  icons: {
    defaultSet: 'mdi', // This is already the default value - only for display purposes
  },
  theme: {
      defaultTheme: 'myCustomLightTheme',
      themes: {
          myCustomLightTheme
      }
  }
});

app.component("QuillEditor", QuillEditor);
app.component("l-map", LMap);
app.component("l-tile-layer", LTileLayer);
app.component("l-marker", LMarker);
app.component("l-tooltip", LTooltip);
app.component("VueDatePicker", VueDatePicker);

app
  .use(router)
  .use(vuetify)
  .use(mdiVue, { icons: mdijs })
  .use(filters)
  .use(Vue3VideoPlayer)
  .use(materialKit)
  // .use(bootstrap)
  .use(store)
  .use(VueSweetalert2)
  .use(VueReCaptcha, {
    siteKey: '6LdtamgpAAAAAAf6mes83zu8IXtO2buwKdAQ7hCX',
    loaderOptions: {
      useRecaptchaNet: true,
      autoHideBadge: false
    }
  })
  .mount("#app");
