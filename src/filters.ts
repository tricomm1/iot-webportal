export default {
    install: (app) => {
        app.config.globalProperties.$filters = {
            maxLengthText(text: string, length: number) {
                if (text.length > length) {
                    return text.slice(0, length - 3) + '...';
                }
                return text;
            }
        };

    }
};